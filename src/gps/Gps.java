/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gps;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 *
 * @author Lorenzo
 */
public class Gps {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, IOException{
        Scanner scan = new Scanner(System.in);
        String latitudine;
        String longitudine;
        System.out.println("Inserisci la latitudine:");
        latitudine = scan.next();
        System.out.println("Inserisci la longitudine:");
        longitudine = scan.next();
        System.out.println("Latitudine: " + latitudine + "\nlongitudine: " + longitudine);
        System.out.println(gpsToAdress(latitudine, longitudine));
    }

    private static String gpsToAdress(String lat, String lng) throws MalformedURLException, IOException {

        URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng="
                + lat + "," + lng + "&sensor=true");
        //System.out.println(url);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        String indirizzo = "";
        int index;
        try {
            InputStream in = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String result, line = reader.readLine();
            result = line;
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }
            index = result.indexOf("\"formatted_address\"")+22;
            indirizzo =  result.substring(index, result.indexOf("\n", index));
        } finally {
            urlConnection.disconnect();
            if(indirizzo.contains("\"status\" : \"ZERO_RESULTS\""))
                throw new IllegalArgumentException("Errore nelle coordinate");
            return indirizzo;
        }
    }
}
